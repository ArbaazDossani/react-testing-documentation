import React from 'react';
import { Drawer, Button, Icon } from 'antd';
import { Link } from "react-router-dom";


class DrawerApp extends React.Component {
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showDrawer}>
            <Icon type="menu-fold" />
        </Button>
        <Drawer
          title="Documentation"
          placement="left"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
            <Link to="/" className="item" onClick={this.onClose}>Home</Link>
            <Link to="/manual" className="item" onClick={this.onClose}>Manual Testing</Link>
            <Link to="/automation" className="item" onClick={this.onClose}>Automation Testing</Link>
        </Drawer>
      </div>
    );
  }
}

export default DrawerApp;