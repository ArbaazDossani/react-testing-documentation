import React from 'react';
import { Link } from "react-router-dom";

class Homepage extends React.Component {
    render() {
        const testingData = this.props.testingData || {};
        return (
            <div className="App">
                <h1>{testingData.appName}</h1>
                <br />
                {
                    testingData.categories && testingData.categories.map((category, i) => {
                    console.warn(category)
                    return(
                        <Link to={category.link} key={i}>
                        <div className="card">
                            <div className="image">
                            <img className="imgs" src={category.image} width="100%" alt="category" />
                            </div>
                            <div className="text">
                            <h3>{category.name}</h3>
                            <p>{category.description}</p>
                            </div>
                        </div>
                        </Link>
                    )
                    })
                }
            </div>
        );
    }
}

export default Homepage;