import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './App.css';
import ManualTesting from './ManualTesting';
import Homepage from './Homepage';
import Drawer from './Drawer';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      testingData: []
    };
  }

  componentDidMount() {
    fetch("https://jsonblob.com/api/jsonBlob/8603a649-ae30-11e9-8313-3db3c3734f56")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            testingData: result[0]
          });
        })
      .catch((err) => {
        console.warn(err);
      }) ;
  }

  render() {
    const testingData = this.state.testingData || {};
    return (
      <React.Fragment>
        <Router>
          <Drawer />
          <Route
            path="/manual"
            component={() => <ManualTesting manualData={testingData.data ? testingData.data[0].data : []} />}
          />
          <Route
            path="/"
            exact
            component={() => <Homepage testingData={testingData} />}
          />
        </Router>
      </React.Fragment>
    );
  }
}

export default App;