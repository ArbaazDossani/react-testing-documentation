import React from 'react';

class ManualTesting extends React.Component {
    render() {
        return (
            <div className="App">
                {
                    this.props.manualData.length > 0 && this.props.manualData.map((manual, i) => {
                        return (
                            <div key={i} className="wrapper">
                                <ul className="mat_list card scrollable">
                                <div className="mat_list_title" style={{ backgroundImage: 'url(https://image.freepik.com/free-vector/shiny-triangles-background_23-2147512500.jpg)' }}>
                                    <h1>{manual.dataType}</h1>
                                </div>
                                {
                                    manual.links.length > 0 && manual.links.map((link, j) => {
                                        return(
                                            <li key={j}><a className="anchor" href={link.url}>{link.name}</a></li>
                                        )
                                    })
                                }
                                </ul>
                            </div>
                        );
                    })
                }
            </div>
        );
    }
}
export default ManualTesting;